# addn

Addn as a dead simple mobile app to fill and sign an 'Attestation de Déplacement Dérogatoire'.

Built in Flutter, this software is distributed "as is" under the GNU GPL v3. Feel free to contact me for any question.

Big thanks to Coralie Orfila for the logo. You can check out her work here : http://www.coralie-orfila.fr

~~I'm currently unable to build and distribute for iOS so if you're willing to help me with that feel free to contact me.~~ iOS version is on the way !