import 'dart:ui' as ui;

import 'package:clay_containers/constants.dart';
import 'package:clay_containers/widgets/clay_containers.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_signature_pad/flutter_signature_pad.dart';
import 'package:pdf/pdf.dart';
import 'package:pdf/widgets.dart' as pw;
import 'package:printing/printing.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Attestation de Déplacement Dérogatoire Numérique',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Attestation de Déplacement Dérogatoire Numérique'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  final _formKey = GlobalKey<FormState>();
  final _signKey = GlobalKey<SignatureState>();

  final List<double> checkmarkOffsets = [-316.0, -378.0, -415.0, -440.0, 375];

  Color customPrimaryColor = Color(0xFF484c7f);

  TextEditingController _nameController = TextEditingController();
  TextEditingController _surnameController = TextEditingController();
  TextEditingController _birthdayController = TextEditingController();
  TextEditingController _addressController = TextEditingController();
  TextEditingController _cityController = TextEditingController();

  int _reason = 0;
  ByteData _signature;

  @override
  void initState() {
    super.initState();

    getSavedValues();

    Future.delayed(Duration.zero, () {
      showAddnDialog(
      "Attention",
      "Cette application a pour but de facilité la création d'une attestation de déplacement dérogatoire.\nN'en abusez pas et sortez uniquement en cas de nécéssité avérée !");
    });

  }

  void getSavedValues() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    setState(() {
      _nameController.text = prefs.getString("name") ?? "";
      _surnameController.text = prefs.getString("surname") ?? "";
      _birthdayController.text = prefs.getString("birthday") ?? "";
      _addressController.text = prefs.getString("address") ?? "";
      _cityController.text = prefs.getString("city") ?? "";
    });
  }

  void saveValues() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    prefs.setString("name", this._nameController.text);
    prefs.setString("surname", this._surnameController.text);
    prefs.setString("birthday", this._birthdayController.text);
    prefs.setString("address", this._addressController.text);
    prefs.setString("city", this._cityController.text);
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      floatingActionButton: ClayContainer(
        borderRadius: 50.0,
        curveType: CurveType.convex,
        emboss: false,
        child: FloatingActionButton(
          child: Icon(Icons.edit, color: Colors.black54,),
          backgroundColor: Colors.transparent,
          elevation: 0,
          onPressed: () {
            if (_formKey.currentState.validate()) {
              showDialog(
                context: context,
                builder: (BuildContext context) {
                  return AlertDialog(
                    title: Text("Veuillez signer ici"),
                    content: Container(
                      width: MediaQuery.of(context).size.width * 0.9,
                      height: 200.0,
                      child: Signature(
                        key: _signKey,
                        color: Colors.black,
                        strokeWidth: 2.5,
                      ),
                    ),
                    actions: <Widget>[
                      FlatButton(
                        child: Text("Annuler", style: TextStyle(color: customPrimaryColor,),),
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                      ),
                      FlatButton(
                        child: Text("Valider", style: TextStyle(color: customPrimaryColor,),),
                        onPressed: () async {
                          final sign = _signKey.currentState;
                          final image = await sign.getData();
                          this._signature = await image.toByteData(format: ui.ImageByteFormat.png);
                          sign.clear();

                          saveValues();

                          Navigator.of(context).pop();
                          showAddnDialog(
                            "Attention",
                            "Depuis le 18 Mars 2020 les attestation sur téléphone portable ne sont plus valable.\nIl est donc nécéssaire d'imprimer le pdf généré, vous pouvez bien sûr le faire depuis l'application",
                            then: () {
                              generatePDF();
                            }
                          );
                          
                        },
                      ),
                    ],
                  );
                },
              );
            }
          },
        ),
      ),
      body: Container(
        child: Form(
          key: _formKey,
          child: GlowingOverscrollIndicator(
            axisDirection: AxisDirection.down,
            color: customPrimaryColor,
            child: ListView(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.symmetric(vertical: 15.0, horizontal: 15.0),
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Flexible(
                        child: Text(widget.title, style: TextStyle(fontSize: 28.0, fontWeight: FontWeight.bold), textAlign: TextAlign.start,),
                      )
                    ],
                  ),
                ),
                Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(top: 20.0, bottom: 10.0),
                      child: ClayContainer(
                        curveType: CurveType.none,
                        borderRadius: 15,
                        child: Container(
                          width: MediaQuery.of(context).size.width * 0.9,
                          height: 60.0,
                          padding: EdgeInsets.only(left: 10.0, top: 6.0),
                          child: TextFormField(
                            decoration: InputDecoration.collapsed(hintText: "Nom"),
                            controller: this._nameController,
                            validator: (value) {
                              if (value.isEmpty) {
                                return 'Veuillez saisir votre nom';
                              }
                              setState(() {
                                this._nameController.text = value;
                              });
                              return null;
                            },
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(top: 10.0, bottom: 10.0),
                      child: ClayContainer(
                        curveType: CurveType.none,
                        borderRadius: 15,
                        child: Container(
                          width: MediaQuery.of(context).size.width * 0.9,
                          height: 60.0,
                          padding: EdgeInsets.only(left: 10.0, top: 6.0),
                          child: TextFormField(
                            decoration: InputDecoration.collapsed(hintText: "Prénom"),
                            controller: this._surnameController,
                            validator: (value) {
                              if (value.isEmpty) {
                                return 'Veuillez saisir votre prénom';
                              }
                              setState(() {
                                this._surnameController.text = value;
                              });
                              return null;
                            },
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(top: 10.0, bottom: 10.0),
                      child: ClayContainer(
                        curveType: CurveType.none,
                        borderRadius: 15,
                        child: Container(
                          width: MediaQuery.of(context).size.width * 0.9,
                          height: 60.0,
                          padding: EdgeInsets.only(left: 10.0, top: 6.0),
                          child: TextFormField(
                            decoration: InputDecoration.collapsed(hintText: "Date de naissance"),
                            controller: this._birthdayController,
                            validator: (value) {
                              if (value.isEmpty) {
                                return 'Veuillez saisir votre date de naissance';
                              }
                              setState(() {
                                this._birthdayController.text = value;
                              });
                              return null;
                            },
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(top: 10.0, bottom: 10.0),
                      child: ClayContainer(
                        curveType: CurveType.none,
                        borderRadius: 15,
                        child: Container(
                          width: MediaQuery.of(context).size.width * 0.9,
                          height: 60.0,
                          padding: EdgeInsets.only(left: 10.0, top: 6.0),
                          child: TextFormField(
                            decoration: InputDecoration.collapsed(hintText: "Adresse"),
                            controller: this._addressController,
                            validator: (value) {
                              if (value.isEmpty) {
                                return 'Veuillez saisir votre adresse';
                              }
                              setState(() {
                                this._addressController.text = value;
                              });
                              return null;
                            },
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(top: 10.0, bottom: 10.0),
                      child: ClayContainer(
                        curveType: CurveType.none,
                        borderRadius: 15,
                        child: Container(
                          width: MediaQuery.of(context).size.width * 0.9,
                          height: 60.0,
                          padding: EdgeInsets.only(left: 10.0, top: 6.0),
                          child: TextFormField(
                            decoration: InputDecoration.collapsed(hintText: "Ville de signature"),
                            controller: this._cityController,
                            validator: (value) {
                              if (value.isEmpty) {
                                return 'Veuillez saisir la ville où vous êtes';
                              }
                              setState(() {
                                this._cityController.text = value;
                              });
                              return null;
                            },
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                Container(
                  margin: EdgeInsets.symmetric(vertical: 20.0),
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Flexible(
                        child: Text("Motif de déplacement :", style: TextStyle(fontSize: 20.0), textAlign: TextAlign.center,),
                      )
                    ],
                  ),
                ),
                ListTile(
                  title: const Text('Activité professionnelle'),
                  leading: Radio(
                    value: 0,
                    groupValue: _reason,
                    activeColor: customPrimaryColor,
                    onChanged: (int value) {
                      setState(() { _reason = value; });
                    },
                  ),
                ),
                ListTile(
                  title: const Text('Achats de première nécessité'),
                  leading: Radio(
                    value: 1,
                    groupValue: _reason,
                    activeColor: customPrimaryColor,
                    onChanged: (int value) {
                      setState(() { _reason = value; });
                    },
                  ),
                ),
                ListTile(
                  title: const Text('Santé'),
                  leading: Radio(
                    value: 2,
                    groupValue: _reason,
                    activeColor: customPrimaryColor,
                    onChanged: (int value) {
                      setState(() { _reason = value; });
                    },
                  ),
                ),
                ListTile(
                  title: const Text('Motif familial impérieux'),
                  leading: Radio(
                    value: 3,
                    groupValue: _reason,
                    activeColor: customPrimaryColor,
                    onChanged: (int value) {
                      setState(() { _reason = value; });
                    },
                  ),
                ),
                ListTile(
                  title: const Text('Déplacement bref à proximité du domicile'),
                  leading: Radio(
                    value: 4,
                    groupValue: _reason,
                    activeColor: customPrimaryColor,
                    onChanged: (int value) {
                      setState(() { _reason = value; });
                    },
                  ),
                ),
                Container(
                  height: 150.0,
                  child: Center(
                    child: Column(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text("Application développée par Primaël Quémerais", textAlign: TextAlign.center,),
                        Container(height: 10.0,),
                        Text("Logo par Coralie Orfila", textAlign: TextAlign.center,),
                        Container(height: 10.0,),
                        Text("Code source disponible :\nhttps://gitlab.com/Reefind/addn", textAlign: TextAlign.center,),
                      ],
                    ),
                  ),
                )
              ],
            ),
          ) 
        ),
      ),
    );
  }

  void generatePDF() async {
    final pdf = pw.Document();

    const backgroundProvider = const AssetImage('assets/add_source.png');
    final PdfImage background = await pdfImageFromImageProvider(pdf: pdf.document, image: backgroundProvider);

    const checkmarkProvider = const AssetImage('assets/checkmark.png');
    final PdfImage checkmark = await pdfImageFromImageProvider(pdf: pdf.document, image: checkmarkProvider);

    ImageProvider signatureProvider = MemoryImage(this._signature.buffer.asUint8List());
    final PdfImage signature = await pdfImageFromImageProvider(pdf: pdf.document, image: signatureProvider);

    var now = new DateTime.now();
    var todayDay = now.day;
    var todayMonth = now.month;

    pdf.addPage(
      pw.Page(
        pageFormat: PdfPageFormat.a4,
        build: (pw.Context context) {
          return pw.Stack(
            children: <pw.Widget>[
              pw.Image(background),
              pw.Transform.translate(
                offset: PdfPoint(100.0, -165.0),
                child: pw.Text("${this._nameController.text} ${this._surnameController.text}", style: pw.TextStyle(fontSize: 12.0)),
              ),
              pw.Transform.translate(
                offset: PdfPoint(100.0, -187.0),
                child: pw.Text("${this._birthdayController.text}", style: pw.TextStyle(fontSize: 12.0)),
              ),
              pw.Transform.translate(
                offset: PdfPoint(110.0, -215.0),
                child: pw.Text("${this._addressController.text}", style: pw.TextStyle(fontSize: 12.0)),
              ),
              pw.Transform.translate(
                offset: PdfPoint(35.0, this.checkmarkOffsets[this._reason]),
                child: pw.Container(
                  width: 24.0,
                  height: 24.0,
                  child: pw.Image(checkmark, fit: pw.BoxFit.contain),
                ),
              ),
              pw.Transform.translate(
                offset: PdfPoint(300.0, -560.0),
                child: pw.Text("${this._cityController.text}", style: pw.TextStyle(fontSize: 8.0)),
              ),
              pw.Transform.translate(
                offset: PdfPoint(390.0, -560.0),
                child: pw.Text("$todayDay", style: pw.TextStyle(fontSize: 8.0)),
              ),
              pw.Transform.translate(
                offset: PdfPoint(410.0, -560.0),
                child: pw.Text("$todayMonth", style: pw.TextStyle(fontSize: 8.0)),
              ),
              pw.Transform.translate(
                offset: PdfPoint(360.0, -585.0),
                child: pw.Container(
                  width: 80.0,
                  height: 80.0,
                  child: pw.Image(signature, fit: pw.BoxFit.contain),
                ),
              ),
            ],
          );
        }
      ),
    );

    await Printing.layoutPdf(onLayout: (PdfPageFormat format) async => pdf.save());
  }

  void showAddnDialog(String title, String content, {Function then}) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return BackdropFilter(
          filter: ui.ImageFilter.blur(sigmaX: 4, sigmaY: 4),
          child: AlertDialog(
            title: Text(title),
            backgroundColor: Colors.white70,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20.0),
            ),
            content: Container(
              width: MediaQuery.of(context).size.width * 0.9,
              height: MediaQuery.of(context).size.height * 0.3,
              child: Center(child: Text(content, textAlign: TextAlign.center,)),
            ),
            actions: <Widget>[
              FlatButton(
                child: Text("C'est compris", style: TextStyle(color: customPrimaryColor,),),
                onPressed: () {
                  Navigator.of(context).pop();
                  if(then != null) {
                    then();
                  }
                },
              ),
            ],
          ),
        );
      },
    );
  }
}
